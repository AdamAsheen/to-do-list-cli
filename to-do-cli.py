#!/usr/bin/env python
import json
import typer
import datetime
from rich import print
from typing_extensions import Annotated
from typing import Optional
from pathlib import Path


app = typer.Typer()
script_dir = Path(__file__).resolve().parent
config_path: Path = script_dir / "tasks.json"

@app.command() 
def add(new_tasks:str,date: Annotated[Optional[str],typer.Option(help = "Will add the date to the task, must be in the format yyyy/month/day")] = None ) -> dict:
    current_tasks = importer()
    task_num = len(current_tasks) + 1
    current_tasks[f"Task {task_num}"] = new_tasks
    
    if date is not None:
        date = date.strip().split("/")
        year,month,day = map(int,date)
        current_tasks[f"Task {task_num}"] = f"{new_tasks} - {datetime.date(year,month,day)}"


    with open(config_path, 'w') as file:
        json.dump(current_tasks, file, indent=4)
    
    return view()

@app.command()
def view():
    data = importer()
    today = datetime.date.today()

    if len(data) == 0:
        print("You don't have any tasks")
        return
    print("Your current tasks are:", end = "\n")
    for key,values in data.items():
        date = values.strip().split("-")
        if len(date) == 1:
            print(f"{key} - {values}")
        else:
            try:
                year,month,day = map(int,date[1:])
                task_date = datetime.date(year,month,day)
            except ValueError:
                print("Error Ocurred")

            formatted_date = task_date.strftime("%d/%m/%Y")
            if  task_date < today:
                print(f"{key} - {date[0]} {formatted_date} OVERDUE")
            elif task_date == today:
                print(f"{key} - {date[0]} {formatted_date} Due Today") 
            else:
                due = task_date - today
                due_date = due.days
                days = "day" if due_date == 1 else "days"
                print(f"{key} - {date[0]} {formatted_date} Due in {due_date} {days}")


    
def importer():
    try:
        with open(config_path, 'r') as file:
            content = file.read()
            if content:
                tasks = json.loads(content)
                return tasks
            else:
                return {}
    except FileNotFoundError:
        return {}
    except json.JSONDecodeError:
        print(f"Error decoding JSON in file: tasks.json")
        return {}

@app.command()
def delete(task_number: Annotated[Optional[str],typer.Option(help = "Remove a specific task number")] = None, clear: Annotated[Optional[bool], typer.Option(help = "Removes all tasks")] = False):
    data = importer()
    new_data = {}
    if clear:
        data.clear()
    else:
        data.pop(f"Task {task_number}", None)

    for index,values in enumerate(data.values(), start = 1):
        new_data[f"Task {index}"] = values

    with open(config_path, "w") as file:
        json.dump(new_data, file, indent = 4)


    return view()

if __name__ == "__main__":
    app()