This is a simple to do list app for the command line. It uses python and the [typer](https://typer.tiangolo.com/) library to create terminal commands and provide a simple ui and formatting. These are a list of commands: 
`to-do view`
`to-do delete`
`to-do add`
`to-do --help`
For optional parameters you can find them on the help page